# /usr/bin/env python
# -*- coding: utf-8 -*-


import subprocess
from typing import List, Dict


class ASError(Exception):
    pass


class Route:
    def __init__(
        self: "Route",
        prefix: str,
        nh: str = None,
        metric: str = None,
        lpref: str = None,
        weight: str = None,
        aspath: str = None,
    ) -> None:
        self.nh = nh
        self.metric = metric
        self.lpref = lpref
        self.weight = weight
        self.aspath = aspath
        if "/" not in prefix:
            self.mask = Route.get_mask(prefix)
            self.prefix = prefix
            self.network = prefix + "/" + str(self.mask)
        else:
            self.mask = int(prefix.split("/")[1])
            self.prefix = prefix.split("/")[0]
            self.network = prefix
        self.bin = self.binary()

    @staticmethod
    def get_mask(prefix: str) -> int:
        """
        Return mask for route if it does not have one in FV table. Based on route class (A,B or C). Returns int.
        """
        f_octet = int(prefix.split(".")[0])
        if f_octet in range(1, 128):
            return 8
        elif f_octet in range(128, 192):
            return 16
        else:
            return 24

    def binary(self: "Route") -> str:
        binary = ""
        for i in self.prefix.split("/")[0].split("."):
            binary += format(int(i), "08b")
        return binary


class OurAddress(Route):
    def __init__(self: "OurAddress", prefix: str) -> None:
        super().__init__(prefix)
        self.bestmatchmask = 0
        self.matched_route = ""
        self.aspath_info = dict()

    def compare(self: "OurAddress", other: "Route") -> None:
        """
        Find best match in FV table for our address. OurAddress object is updated with best route and it's attributes.
        """
        bestmatchmask = 0
        flag = False
        if not isinstance(other, Route):
            raise ValueError("Object should be from Route class")
        if self.bin[: other.mask] == other.bin[: other.mask]:
            flag = True
        if flag and self.bestmatchmask < other.mask:
            self.bestmatchmask = other.mask
            self.matched_route = other.network
            self.nh = other.nh
            self.metric = other.metric
            self.lpref = other.lpref
            self.weight = other.weight
            self.aspath = other.aspath

    def get_info(self: "OurAddress") -> List[Dict]:
        """
        Get information about each ASN in AS-Path from registers. Returns List of Dicts.
        """
        if not self.aspath:
            print("ASPath attribute is empty. Route is locally originated")
            return None
        self.aspath_info = []
        prev_asn = ""
        for asn in self.aspath.split():
            if asn != prev_asn:
                prev_asn = asn
                self.aspath_info.append({"asn": asn})
                command_result = subprocess.run(
                    f"whois AS{asn}", shell=True, stdout=subprocess.PIPE
                )
                if command_result.returncode == 0:
                    output = command_result.stdout.decode().split("\n")
                else:
                    raise ASError(f"Could not get information for as AS{asn}")
                for line in output:
                    if any(x in line for x in ("as-name", "ASName")):
                        self.aspath_info[-1].update({"as-name": line.split()[1]})
                    if any(x in line for x in ("org-name", "OrgName")):
                        self.aspath_info[-1].update(
                            {"org-name": line.split(maxsplit=1)[1]}
                        )
                    if any(x in line for x in ("Country", "country")):
                        self.aspath_info[-1].update(
                            {"country": line.split(maxsplit=1)[1]}
                        )
                self.aspath_info = self.aspath_info
        return self.aspath_info

