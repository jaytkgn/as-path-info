# /usr/bin/env python
# -*- coding: utf-8 -*-

import re
from typing import List

from routeclasses import Route, OurAddress

regex_full_line = re.compile(
    r"\*\>\w[\d\.\/]* +(?P<NH>\S+) +(?P<METRIC>\d+)* +(?P<LPREF>\d{2,3}) +(?P<WGHT>\d+) *(?P<ASPATH>(?:\d+ )*\d+)* \S"
)
regex_part_line = re.compile(r"\*[>\s][ie\?](?P<PREFIX>\S+)")


def input_address() -> str:
    """
    IPv4 address input with validation. Returns string.
    """
    flag = False
    while not flag:
        ouraddress = input("Input valid ipv4 address: ")
        flag = True
        if len(ouraddress.split(".")) != 4:
            flag = False
        else:
            for octet in ouraddress.split("."):
                if octet.isdigit():
                    if int(octet) < 0 or int(octet) > 255:
                        flag = False
                        break
                else:
                    flag = False
                    break
        if not flag:
            print("Not valid address.")
    return ouraddress


def get_route_recs(filename: str) -> str:
    """
    Get block for one route with all possible NH from FV table. Yields one string.
    """
    route_rec = ""
    line = ""
    with open(filename) as f:
        line = f.readline()
        if not re.search(regex_part_line, line):
            while not re.search(regex_part_line, line):
                line = f.readline()
        route_rec += line
        line = f.readline()
        while True:
            while not re.search(regex_part_line, line) and line.rstrip():
                route_rec += line
                line = f.readline()
            yield route_rec
            if not line.rstrip():
                return
            route_rec = line
            line = f.readline()


def get_routes(route_rec: str) -> Route:
    """
    Turn one route string block into Route object (best route is taken only). Returns Route object.
    """

    def parse_line(line):
        m = re.search(regex_full_line, line)
        if m:
            route = Route(prefix, *m.groupdict().values())
            return route
        return None

    partial = ""
    result = ""
    flag = False
    route_rec = route_rec.split("\n")
    m = re.search(regex_part_line, route_rec[0])
    prefix = m.group("PREFIX")
    for line in route_rec:
        if (
            partial
        ):  # Here it is done like that in case if Best route string is splitted on two strings.
            partial += line
            result = parse_line(partial)
        if "*>" in line:
            result = parse_line(line)
            if not result:
                partial += line
        if result:
            partial = ""
            return result


if __name__ == "__main__":
    ouraddress = OurAddress(input_address())
    filename = "session.log"
    gen = get_route_recs(filename)
    for i in gen:
        ouraddress.compare(get_routes(i))
    print(f"Route for address {ouraddress.prefix} is {ouraddress.matched_route}")
    print(f"AS-PATH is following: {ouraddress.aspath}")
    print(f"AS-PATH information:")
    as_info = ouraddress.get_info()
    if as_info:
        for i in as_info:
            print(i)
